
var navMain = document.querySelector('.page__nav');
var navToggle = document.querySelector('.page__nav__toggle')
navMain.classList.remove('page__nav--nojs')
navToggle.addEventListener('click', function() {
  if (navMain.classList.contains('page__nav--closed')) {
    navMain.classList.remove('page__nav--closed');
    navMain.classList.add('page__nav--opened');
  } else {
    navMain.classList.add('page__nav--closed');
    navMain.classList.remove('page__nav--opened');
  }
}
);
    